const canvas = document.querySelector('#draw');
const frame = document.querySelector('.frame');
canvas.width = frame.clientWidth;
canvas.height = frame.clientHeight;

const ctx = canvas.getContext('2d');
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 10;

const clear_button = document.querySelector('button');

let isDrawing = false;
let lastX = 0;
let lastY = 0;
let hue = 0;
let direction = true;

function draw(e) {
	if (!isDrawing) return;

	ctx.strokeStyle = 'hsl(' + hue + ', 100%, 50%)';
	ctx.beginPath();
	ctx.moveTo(lastX, lastY);
	ctx.lineTo(e.offsetX, e.offsetY);
	ctx.stroke();

	[lastX, lastY] = [e.offsetX, e.offsetY];
	hue++;

	if (ctx.lineWidth > 100 || ctx.lineWidth <= 1) direction = !direction;
	if (direction) ctx.lineWidth++;
	else ctx.lineWidth--;
}

function clear() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}

canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mousedown', (e) => {
	isDrawing = true;
	[lastX, lastY] = [e.offsetX, e.offsetY];
});
canvas.addEventListener('mouseup', () => isDrawing = false);
canvas.addEventListener('mouseout', () => isDrawing = false);

clear_button.addEventListener('click', clear);

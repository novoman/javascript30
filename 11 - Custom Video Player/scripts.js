const player = document.querySelector('.player');
const video = player.querySelector('.viewer');
const progress = player.querySelector('.progress');
const progressBar = player.querySelector('.progress__filled');
const toggle = player.querySelector('.toggle');
const skipButtons = player.querySelectorAll('[data-skip]');
const rangeButtons = player.querySelectorAll('.player__slider');
const fullScreenButton = player.querySelector('#full_screen');

function tooglePlay() {
	video.paused ? video.play() : video.pause();
}

function updateButton() {
	toggle.textContent = this.paused ? '►' : '❚❚';
}

function skip() {
	video.currentTime += parseFloat(this.dataset.skip);
}

function handleRangeUpdate() {
	video[this.name] = this.value;
}

function handleProgress() {
	progressBar.style.flexBasis = (video.currentTime / video.duration) * 100 + '%';
}

function scrub(e) {
	video.currentTime = (e.offsetX / progress.offsetWidth) * video.duration
}

function handleScreenSize() {
	if (video.requestFullscreen) {
		video.requestFullscreen();
	} else if (video.mozRequestFullScreen) { /* Firefox */
		video.mozRequestFullScreen();
	} else if (video.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
		video.webkitRequestFullscreen();
	} else if (video.msRequestFullscreen) { /* IE/Edge */
		video.msRequestFullscreen();
	}
}

toggle.addEventListener('click', tooglePlay);
video.addEventListener('click', tooglePlay);
video.addEventListener('play', updateButton);
video.addEventListener('pause', updateButton);

skipButtons.forEach(button => button.addEventListener('click', skip));
rangeButtons.forEach(button => button.addEventListener('change', handleRangeUpdate));
rangeButtons.forEach(button => button.addEventListener('mousemove', handleRangeUpdate));

video.addEventListener('timeupdate', handleProgress);
progress.addEventListener('click', scrub);

fullScreenButton.addEventListener('click', handleScreenSize);
